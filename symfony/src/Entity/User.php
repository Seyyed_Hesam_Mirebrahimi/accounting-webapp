<?php
namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="`user`")
 */

class User extends BaseUser{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Budgets", mappedBy="user")
     */
    private $budgets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Transactions", mappedBy="user")
     */
    private $transactions;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserPeople", mappedBy="user")
     */
    private $userPeople;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LandBorrow", mappedBy="user")
     */
    private $landBorrows;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Incomes", mappedBy="user")
     */
    private $incomes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TransactionsCategories", mappedBy="createdBy")
     */
    private $transactionsCategories;


    public function __construct()
    {
        parent::__construct();
        $this->budgets = new ArrayCollection();
        $this->transactions = new ArrayCollection();
        $this->userPeople = new ArrayCollection();
        $this->landBorrows = new ArrayCollection();
        $this->incomes = new ArrayCollection();
        $this->transactionsCategories = new ArrayCollection();
    }

    /**
     * @return Collection|Budgets[]
     */
    public function getBudgets(): Collection
    {
        return $this->budgets;
    }

    public function addBudget(Budgets $budget): self
    {
        if (!$this->budgets->contains($budget)) {
            $this->budgets[] = $budget;
            $budget->setUser($this);
        }

        return $this;
    }

    public function removeBudget(Budgets $budget): self
    {
        if ($this->budgets->contains($budget)) {
            $this->budgets->removeElement($budget);
            // set the owning side to null (unless already changed)
            if ($budget->getUser() === $this) {
                $budget->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Transactions[]
     */
    public function getTransactions(): Collection
    {
        return $this->transactions;
    }

    public function addTransaction(Transactions $transaction): self
    {
        if (!$this->transactions->contains($transaction)) {
            $this->transactions[] = $transaction;
            $transaction->setUser($this);
        }

        return $this;
    }

    public function removeTransaction(Transactions $transaction): self
    {
        if ($this->transactions->contains($transaction)) {
            $this->transactions->removeElement($transaction);
            // set the owning side to null (unless already changed)
            if ($transaction->getUser() === $this) {
                $transaction->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserPeople[]
     */
    public function getUserPeople(): Collection
    {
        return $this->userPeople;
    }

    public function addUserPerson(UserPeople $userPerson): self
    {
        if (!$this->userPeople->contains($userPerson)) {
            $this->userPeople[] = $userPerson;
            $userPerson->setUser($this);
        }

        return $this;
    }

    public function removeUserPerson(UserPeople $userPerson): self
    {
        if ($this->userPeople->contains($userPerson)) {
            $this->userPeople->removeElement($userPerson);
            // set the owning side to null (unless already changed)
            if ($userPerson->getUser() === $this) {
                $userPerson->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|LandBorrow[]
     */
    public function getLandBorrows(): Collection
    {
        return $this->landBorrows;
    }

    public function addLandBorrow(LandBorrow $landBorrow): self
    {
        if (!$this->landBorrows->contains($landBorrow)) {
            $this->landBorrows[] = $landBorrow;
            $landBorrow->setUser($this);
        }

        return $this;
    }

    public function removeLandBorrow(LandBorrow $landBorrow): self
    {
        if ($this->landBorrows->contains($landBorrow)) {
            $this->landBorrows->removeElement($landBorrow);
            // set the owning side to null (unless already changed)
            if ($landBorrow->getUser() === $this) {
                $landBorrow->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Incomes[]
     */
    public function getIncomes(): Collection
    {
        return $this->incomes;
    }

    public function addIncome(Incomes $income): self
    {
        if (!$this->incomes->contains($income)) {
            $this->incomes[] = $income;
            $income->setUser($this);
        }

        return $this;
    }

    public function removeIncome(Incomes $income): self
    {
        if ($this->incomes->contains($income)) {
            $this->incomes->removeElement($income);
            // set the owning side to null (unless already changed)
            if ($income->getUser() === $this) {
                $income->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TransactionsCategories[]
     */
    public function getTransactionsCategories(): Collection
    {
        return $this->transactionsCategories;
    }

    public function addTransactionsCategory(TransactionsCategories $transactionsCategory): self
    {
        if (!$this->transactionsCategories->contains($transactionsCategory)) {
            $this->transactionsCategories[] = $transactionsCategory;
            $transactionsCategory->setCreatedBy($this);
        }

        return $this;
    }

    public function removeTransactionsCategory(TransactionsCategories $transactionsCategory): self
    {
        if ($this->transactionsCategories->contains($transactionsCategory)) {
            $this->transactionsCategories->removeElement($transactionsCategory);
            // set the owning side to null (unless already changed)
            if ($transactionsCategory->getCreatedBy() === $this) {
                $transactionsCategory->setCreatedBy(null);
            }
        }

        return $this;
    }





}
