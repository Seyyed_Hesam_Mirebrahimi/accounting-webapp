<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserPeopleRepository")
 */
class UserPeople
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userPeople")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\LandBorrow", mappedBy="people")
     */
    private $landBorrows;

    public function __construct()
    {
        $this->landBorrows = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return Collection|LandBorrow[]
     */
    public function getLandBorrows(): Collection
    {
        return $this->landBorrows;
    }

    public function addLandBorrow(LandBorrow $landBorrow): self
    {
        if (!$this->landBorrows->contains($landBorrow)) {
            $this->landBorrows[] = $landBorrow;
            $landBorrow->setPeople($this);
        }

        return $this;
    }

    public function removeLandBorrow(LandBorrow $landBorrow): self
    {
        if ($this->landBorrows->contains($landBorrow)) {
            $this->landBorrows->removeElement($landBorrow);
            // set the owning side to null (unless already changed)
            if ($landBorrow->getPeople() === $this) {
                $landBorrow->setPeople(null);
            }
        }

        return $this;
    }
}
