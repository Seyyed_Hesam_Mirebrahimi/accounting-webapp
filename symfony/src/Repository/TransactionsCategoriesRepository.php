<?php

namespace App\Repository;

use App\Entity\TransactionsCategories;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TransactionsCategories|null find($id, $lockMode = null, $lockVersion = null)
 * @method TransactionsCategories|null findOneBy(array $criteria, array $orderBy = null)
 * @method TransactionsCategories[]    findAll()
 * @method TransactionsCategories[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionsCategoriesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TransactionsCategories::class);
    }

    // /**
    //  * @return TransactionsCategories[] Returns an array of TransactionsCategories objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TransactionsCategories
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
