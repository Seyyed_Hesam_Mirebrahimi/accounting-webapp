<?php

namespace App\Repository;

use App\Entity\UserPeople;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method UserPeople|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserPeople|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserPeople[]    findAll()
 * @method UserPeople[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserPeopleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, UserPeople::class);
    }

    // /**
    //  * @return UserPeople[] Returns an array of UserPeople objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserPeople
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
