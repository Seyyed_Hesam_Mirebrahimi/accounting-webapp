<?php

namespace App\Repository;

use App\Entity\Incomes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Incomes|null find($id, $lockMode = null, $lockVersion = null)
 * @method Incomes|null findOneBy(array $criteria, array $orderBy = null)
 * @method Incomes[]    findAll()
 * @method Incomes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IncomesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Incomes::class);
    }

    public function totalTransActionsPrice($start, $end, $userId)
    {

        try{
            return $this->createQueryBuilder('i')
                ->select('sum(i.price) as sum')
                ->join('i.user' , 'user')
                ->andWhere('user.id = :userId')->setParameter('userId' , $userId)
                ->andWhere('i.createdAt >= :start')->setParameter('start' , $start)
                ->andWhere('i.createdAt <= :end')->setParameter('end' , $end)
                ->getQuery()
                ->getArrayResult()[0]['sum']
                ;
        }catch (\Exception $exception){
            return 0 ;
        }

    }

    // /**
    //  * @return Incomes[] Returns an array of Incomes objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Incomes
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
