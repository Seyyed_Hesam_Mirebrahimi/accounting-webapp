<?php

namespace App\Repository;

use App\Entity\LandBorrow;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LandBorrow|null find($id, $lockMode = null, $lockVersion = null)
 * @method LandBorrow|null findOneBy(array $criteria, array $orderBy = null)
 * @method LandBorrow[]    findAll()
 * @method LandBorrow[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LandBorrowRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LandBorrow::class);
    }

    // /**
    //  * @return LandBorrow[] Returns an array of LandBorrow objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LandBorrow
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
