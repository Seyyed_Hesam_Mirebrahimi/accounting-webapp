<?php

namespace App\Repository;

use App\Entity\Transactions;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Transactions|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transactions|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transactions[]    findAll()
 * @method Transactions[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Transactions::class);
    }

    public function totalTransActionsPrice($start , $end , $userId)
    {
     try{
         return $this->createQueryBuilder('t')
             ->select('sum(t.price) as sum')
             ->join('t.user' , 'user')
             ->andWhere('user.id = :userId')->setParameter('userId' , $userId)
             ->andWhere('t.createdAt >= :start')->setParameter('start' , $start)
             ->andWhere('t.createdAt <= :end')->setParameter('end' , $end)
             ->getQuery()
             ->getArrayResult()[0]['sum']
             ;
     }catch (\Exception $exception){
         return 0 ;
     }
    }

    // /**
    //  * @return Transactions[] Returns an array of Transactions objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Transactions
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
