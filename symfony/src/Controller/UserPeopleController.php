<?php

namespace App\Controller;

use App\Entity\UserPeople;
use App\Form\UserPeopleType;
use App\Repository\UserPeopleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/user/people")
 */
class UserPeopleController extends AbstractController
{
    /**
     * @Route("/", name="user_people_index", methods={"GET"})
     * @param UserPeopleRepository $userPeopleRepository
     * @return Response
     */
    public function index(UserPeopleRepository $userPeopleRepository): Response
    {
        return $this->render('user_people/index.html.twig', [
            'user_peoples' => $userPeopleRepository->findBy(['user' => $this->getUser()]),
        ]);
    }

    /**
     * @Route("/new", name="user_people_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $userPerson = new UserPeople();
        $form = $this->createForm(UserPeopleType::class, $userPerson);
        $form->remove('user');
        $userPerson->setUser($this->getUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($userPerson);
            $entityManager->flush();

            return $this->redirectToRoute('user_people_index');
        }

        return $this->render('user_people/new.html.twig', [
            'user_person' => $userPerson,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="user_people_edit", methods={"GET","POST"})
     * @param Request $request
     * @param UserPeople $userPerson
     * @return Response
     */
    public function edit(Request $request, UserPeople $userPerson): Response
    {
        if ($userPerson->getUser() != $this->getUser()){
            throw new AccessDeniedException();
        }
        $form = $this->createForm(UserPeopleType::class, $userPerson);
        $form->remove('user');
        $userPerson->setUser($this->getUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_people_index', [
                'id' => $userPerson->getId(),
            ]);
        }

        return $this->render('user_people/edit.html.twig', [
            'user_person' => $userPerson,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_people_delete", methods={"DELETE"})
     * @param Request $request
     * @param UserPeople $userPerson
     * @return Response
     */
    public function delete(Request $request, UserPeople $userPerson): Response
    {
        if ($this->isCsrfTokenValid('delete'.$userPerson->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($userPerson);
            $entityManager->flush();
        }

        return $this->redirectToRoute('user_people_index');
    }
}
