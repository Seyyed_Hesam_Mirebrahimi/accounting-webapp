<?php

namespace App\Controller;

use App\Entity\Budgets;
use App\Entity\Transactions;
use App\Entity\TransactionsCategories;
use App\Form\TransactionsType;
use App\Repository\BudgetsRepository;
use App\Repository\TransactionsCategoriesRepository;
use App\Repository\TransactionsRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/transactions")
 */
class TransactionsController extends AbstractController
{
    /**
     * @Route("/", name="transactions_index", methods={"GET"})
     * @param TransactionsRepository $transactionsRepository
     * @return Response
     */
    public function index(TransactionsRepository $transactionsRepository): Response
    {
        return $this->render('transactions/index.html.twig', [
            'transactions' => $transactionsRepository->findBy(['user' => $this->getUser()]),
        ]);
    }

    /**
     * @Route("/new", name="transactions_new", methods={"GET","POST"})
     * @param Request $request
     * @param BudgetsRepository $budgetsRepository
     * @param TransactionsCategoriesRepository $categoriesRepository
     * @return Response
     */
    public function new(Request $request, BudgetsRepository $budgetsRepository , TransactionsCategoriesRepository $categoriesRepository): Response
    {
        $transaction = new Transactions();
        $form = $this->createForm(TransactionsType::class, $transaction);
        $form->remove('budget');
        $form->remove('user');
        $form->add('budget', EntityType::class, [
            'required' => 0,
            'label' => 'انتخاب بودجه',
            'class' => Budgets::class,
            'choices' => $budgetsRepository->findBy(['user' => $this->getUser()])
        ]);
        $form->add('category', EntityType::class, [
            'label'=> 'انتخاب دسته بندی',
            'class' => TransactionsCategories::class,
            'choices' => $categoriesRepository->findBy(['createdBy' => $this->getUser()])
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
//            var_dump($this->getUser()->getUsername());die;
            $transaction->setUser($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($transaction);
            $entityManager->flush();

            return $this->redirectToRoute('transactions_index');
        }

        return $this->render('transactions/new.html.twig', [
            'transaction' => $transaction,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="transactions_show", methods={"GET"})
     * @param Transactions $transaction
     * @return Response
     */
    public function show(Transactions $transaction): Response
    {
        if (!$transaction->getUser() == $transaction->getUser()) {
            throw new AccessDeniedException();
        }
        return $this->render('transactions/show.html.twig', [
            'transaction' => $transaction,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="transactions_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Transactions $transaction
     * @param BudgetsRepository $budgetsRepository
     * @return Response
     */
    public function edit(Request $request, Transactions $transaction, BudgetsRepository $budgetsRepository): Response
    {
        if (!$transaction->getUser() == $transaction->getUser()) {
            throw new AccessDeniedException();
        }
        $form = $this->createForm(TransactionsType::class, $transaction);
        $form->remove('budget');
        $form->remove('user');
        $transaction->setUser($transaction->getUser());
        $form->add('budget', EntityType::class, [
            'class' => Budgets::class,
            'label' => 'انتخاب بودجه',
            'choices' => $budgetsRepository->findBy(['user' => $this->getUser()])
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('transactions_index', [
                'id' => $transaction->getId(),
            ]);
        }

        return $this->render('transactions/edit.html.twig', [
            'transaction' => $transaction,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="transactions_delete", methods={"DELETE"})
     * @param Request $request
     * @param Transactions $transaction
     * @return Response
     */
    public function delete(Request $request, Transactions $transaction): Response
    {
//        if ($this->isCsrfTokenValid('delete'.$transaction->getId(), $request->request->get('_token'))) {
//            $entityManager = $this->getDoctrine()->getManager();
//            $entityManager->remove($transaction);
//            $entityManager->flush();
//        }

        return $this->redirectToRoute('transactions_index');
    }
}
