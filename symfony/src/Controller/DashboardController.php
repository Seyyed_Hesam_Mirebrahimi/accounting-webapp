<?php

namespace App\Controller;

use App\DependencyInjection\jDateTime;
use App\DependencyInjection\JDF;
use App\DependencyInjection\shamsi;
use App\Repository\BudgetsRepository;
use App\Repository\IncomesRepository;
use App\Repository\TransactionsRepository;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\User\UserInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class DashboardController extends AbstractController
{
    /**
     * @Route("/dashboard", name="dashboard")
     * @param TransactionsRepository $transactionsRepository
     * @param IncomesRepository $incomesRepository
     * @param BudgetsRepository $budgetsRepository
     * @return Response
     */
    public function mainDashboard(TransactionsRepository $transactionsRepository , IncomesRepository $incomesRepository , BudgetsRepository $budgetsRepository)
    {
        $shamsi = new shamsi();
        $date = $shamsi->getMonthRange();
        $totalTransActionsPrice = $transactionsRepository->totalTransActionsPrice($date['start'] , $date['end'] , $this->getUser()->getId());
        $totalIncomesPrice = $incomesRepository->totalTransActionsPrice($date['start'] , $date['end'] , $this->getUser()->getId());
        $budgetsGoesToTemplate = [];

        foreach ($budgetsRepository->findBy(['user' => $this->getUser()]) as $budgetObject){
            $title = $budgetObject->getName();
            $total = $budgetObject->getMaxBudget();
            $budgetTransActions = $transactionsRepository->findBy([
                'user' => $this->getUser(),
                'budget'=>$budgetObject,

            ]);
            $totalBudgetTransActionCount = 0;
            foreach ($budgetTransActions as $transAction){
                if ($transAction->getCreatedAt() >= $date['start'] && $transAction->getCreatedAt() <= $date['end']){
                    $totalBudgetTransActionCount = $totalBudgetTransActionCount + $transAction->getPrice();
                }
            }

            if ($totalBudgetTransActionCount > $budgetObject->getMaxBudget()){
                $cssClass = 'danger';
                $percent = 100;
            }else{
//                var_dump($totalBudgetTransActionCount);die;
                if ($totalBudgetTransActionCount == 0){
                    $cssClass = 'success';
                    $percent = 0;
                }else{
                    $percent = ($totalBudgetTransActionCount/$budgetObject->getMaxBudget())*100;
                    $cssClass = 'success';
                }

            }
            array_push($budgetsGoesToTemplate , [
                'title' => $title,
                'percent' => $percent,
                'cssClass' => $cssClass,
                'total' => $total,
                'used' => $totalBudgetTransActionCount
            ]);
        }




        return $this->render('dashboard/dashboard.html.twig',[
            'totalTransActionsPrice' => $totalTransActionsPrice,
            'totalIncomesPrice' => $totalIncomesPrice,
            'currentBudget' => $totalIncomesPrice-$totalTransActionsPrice,
            'budgets' => $budgetsGoesToTemplate
        ]);
    }
    /**
     * @Route("/dev", name="dev")
     */
    public function dev()
    {
        $shamsi = new shamsi();
        var_dump($shamsi->getMonthRange());die;
    }
}
