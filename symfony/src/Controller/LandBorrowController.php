<?php

namespace App\Controller;

use App\Entity\LandBorrow;
use App\Entity\UserPeople;
use App\Form\LandBorrowType;
use App\Repository\LandBorrowRepository;
use App\Repository\UserPeopleRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/land/borrow")
 */
class LandBorrowController extends AbstractController
{
    /**
     * @Route("/", name="land_borrow_index", methods={"GET"})
     * @param LandBorrowRepository $landBorrowRepository
     * @return Response
     */
    public function index(LandBorrowRepository $landBorrowRepository): Response
    {
        return $this->render('land_borrow/index.html.twig', [
            'land_borrows' => $landBorrowRepository->findBy(['user' => $this->getUser()]),
        ]);
    }

    /**
     * @Route("/new", name="land_borrow_new", methods={"GET","POST"})
     * @param Request $request
     * @param UserPeopleRepository $peopleRepository
     * @return Response
     */
    public function new(Request $request , UserPeopleRepository $peopleRepository): Response
    {
        $landBorrow = new LandBorrow();
        $form = $this->createForm(LandBorrowType::class, $landBorrow);
        $form->remove('user');
        $form->remove('people');
        $landBorrow->setUser($this->getUser());
        $form->add('people' , EntityType::class , [
            'required' => true,
            'label' => 'فرد مورد نظر',
            'class' => UserPeople::class,
            'choices' =>$peopleRepository->findBy(['user' => $this->getUser()])
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($landBorrow);
            $entityManager->flush();

            return $this->redirectToRoute('land_borrow_index');
        }

        return $this->render('land_borrow/new.html.twig', [
            'land_borrow' => $landBorrow,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="land_borrow_show", methods={"GET"})
     * @param LandBorrow $landBorrow
     * @return Response
     */
    public function show(LandBorrow $landBorrow): Response
    {
        if ($landBorrow->getUser() != $this->getUser()){
            throw new AccessDeniedException();
        }
        return $this->render('land_borrow/show.html.twig', [
            'land_borrow' => $landBorrow,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="land_borrow_edit", methods={"GET","POST"})
     * @param Request $request
     * @param LandBorrow $landBorrow
     * @param UserPeopleRepository $peopleRepository
     * @return Response
     */
    public function edit(Request $request, LandBorrow $landBorrow , UserPeopleRepository $peopleRepository): Response
    {
        if ($landBorrow->getUser() != $this->getUser()){
            throw new AccessDeniedException();
        }
        $form = $this->createForm(LandBorrowType::class, $landBorrow);
        $form->remove('user');
        $form->remove('people');
        $landBorrow->setUser($this->getUser());
        $form->add('people' , EntityType::class , [
            'required' => true,
            'class' => UserPeople::class,
            'label' => 'فرد مورد نظر',
            'choices' =>$peopleRepository->findBy(['user' => $this->getUser()])
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('land_borrow_index', [
                'id' => $landBorrow->getId(),
            ]);
        }

        return $this->render('land_borrow/edit.html.twig', [
            'land_borrow' => $landBorrow,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="land_borrow_delete", methods={"DELETE"})
     * @param Request $request
     * @param LandBorrow $landBorrow
     * @return Response
     */
    public function delete(Request $request, LandBorrow $landBorrow): Response
    {
//        if ($this->isCsrfTokenValid('delete'.$landBorrow->getId(), $request->request->get('_token'))) {
//            $entityManager = $this->getDoctrine()->getManager();
//            $entityManager->remove($landBorrow);
//            $entityManager->flush();
//        }

        return $this->redirectToRoute('land_borrow_index');
    }
}
