<?php

namespace App\Controller;

use App\Entity\Budgets;
use App\Form\BudgetsType;
use App\Repository\BudgetsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/budgets")
 */
class BudgetsController extends AbstractController
{
    /**
     * @Route("/", name="budgets_index", methods={"GET"})
     * @param BudgetsRepository $budgetsRepository
     * @return Response
     */
    public function index(BudgetsRepository $budgetsRepository): Response
    {
        return $this->render('budgets/index.html.twig', [
            'budgets' => $budgetsRepository->findBy(['user' => $this->getUser()]),
        ]);
    }

    /**
     * @Route("/new", name="budgets_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $budget = new Budgets();
        $form = $this->createForm(BudgetsType::class, $budget);
        $form->remove('user');
        $budget->setUser($this->getUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($budget);
            $entityManager->flush();

            return $this->redirectToRoute('budgets_index');
        }

        return $this->render('budgets/new.html.twig', [
            'budget' => $budget,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="budgets_show", methods={"GET"})
     * @param Budgets $budget
     * @return Response
     */
    public function show(Budgets $budget): Response
    {
        if (!$budget->getUser() == $this->getUser()){
            throw new AccessDeniedException();
        }
        return $this->render('budgets/show.html.twig', [
            'budget' => $budget,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="budgets_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Budgets $budget
     * @return Response
     */
    public function edit(Request $request, Budgets $budget): Response
    {
        $form = $this->createForm(BudgetsType::class, $budget);
        $form->remove('user');
        $budget->setUser($this->getUser());

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('budgets_index', [
                'id' => $budget->getId(),
            ]);
        }

        return $this->render('budgets/edit.html.twig', [
            'budget' => $budget,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="budgets_delete", methods={"DELETE"})
     * @param Request $request
     * @param Budgets $budget
     * @return Response
     */
    public function delete(Request $request, Budgets $budget): Response
    {
//        if ($this->isCsrfTokenValid('delete'.$budget->getId(), $request->request->get('_token'))) {
//            $entityManager = $this->getDoctrine()->getManager();
//            $entityManager->remove($budget);
//            $entityManager->flush();
//        }

        return $this->redirectToRoute('budgets_index');
    }
}
