<?php

namespace App\Controller;

use App\Entity\TransactionsCategories;
use App\Form\TransactionsCategoriesType;
use App\Repository\TransactionsCategoriesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/transactions-categories")
 */
class TransactionsCategoriesController extends AbstractController
{
    /**
     * @Route("/", name="transactions_categories_index", methods={"GET"})
     */
    public function index(TransactionsCategoriesRepository $transactionsCategoriesRepository): Response
    {
        return $this->render('transactions_categories/index.html.twig', [
            'transactions_categories' => $transactionsCategoriesRepository->findBy([
                'createdBy' => $this->getUser()
            ]),
        ]);
    }

    /**
     * @Route("/new", name="transactions_categories_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $transactionsCategory = new TransactionsCategories();
        $form = $this->createForm(TransactionsCategoriesType::class, $transactionsCategory);
        $form->remove('createdBy');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $transactionsCategory->setCreatedBy($this->getUser());
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($transactionsCategory);
            $entityManager->flush();

            return $this->redirectToRoute('transactions_categories_index');
        }

        return $this->render('transactions_categories/new.html.twig', [
            'transactions_category' => $transactionsCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="transactions_categories_show", methods={"GET"})
     */
    public function show(TransactionsCategories $transactionsCategory): Response
    {
        return $this->render('transactions_categories/show.html.twig', [
            'transactions_category' => $transactionsCategory,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="transactions_categories_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, TransactionsCategories $transactionsCategory): Response
    {
        $form = $this->createForm(TransactionsCategoriesType::class, $transactionsCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('transactions_categories_index', [
                'id' => $transactionsCategory->getId(),
            ]);
        }

        return $this->render('transactions_categories/edit.html.twig', [
            'transactions_category' => $transactionsCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="transactions_categories_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TransactionsCategories $transactionsCategory): Response
    {
        if ($this->isCsrfTokenValid('delete'.$transactionsCategory->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($transactionsCategory);
            $entityManager->flush();
        }

        return $this->redirectToRoute('transactions_categories_index');
    }
}
