<?php

namespace App\Controller;

use App\Entity\Incomes;
use App\Form\IncomesType;
use App\Repository\IncomesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/incomes")
 */
class IncomesController extends AbstractController
{
    /**
     * @Route("/", name="incomes_index", methods={"GET"})
     * @param IncomesRepository $incomesRepository
     * @return Response
     */
    public function index(IncomesRepository $incomesRepository): Response
    {
        return $this->render('incomes/index.html.twig', [
            'incomes' => $incomesRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="incomes_new", methods={"GET","POST"})
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $income = new Incomes();
        $income->setUser($this->getUser());
        $form = $this->createForm(IncomesType::class, $income);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($income);
            $entityManager->flush();

            return $this->redirectToRoute('incomes_index');
        }

        return $this->render('incomes/new.html.twig', [
            'income' => $income,
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/{id}/edit", name="incomes_edit", methods={"GET","POST"})
     * @param Request $request
     * @param Incomes $income
     * @return Response
     */
    public function edit(Request $request, Incomes $income): Response
    {
        if ($income->getUser() != $this->getUser()){
            throw new AccessDeniedException();
        }
        $form = $this->createForm(IncomesType::class, $income);
        $income->setUser($this->getUser());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('incomes_index', [
                'id' => $income->getId(),
            ]);
        }

        return $this->render('incomes/edit.html.twig', [
            'income' => $income,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="incomes_delete", methods={"DELETE"})
     * @param Request $request
     * @param Incomes $income
     * @return Response
     */
    public function delete(Request $request, Incomes $income): Response
    {
//        if ($this->isCsrfTokenValid('delete'.$income->getId(), $request->request->get('_token'))) {
//            $entityManager = $this->getDoctrine()->getManager();
//            $entityManager->remove($income);
//            $entityManager->flush();
//        }

        return $this->redirectToRoute('incomes_index');
    }
}
