<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190606110557 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE land_borrow (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, people_id INT NOT NULL, note LONGTEXT NOT NULL, type VARCHAR(255) NOT NULL, is_pass TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, pass_at DATETIME DEFAULT NULL, INDEX IDX_FEE8BE67A76ED395 (user_id), INDEX IDX_FEE8BE673147C936 (people_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE land_borrow ADD CONSTRAINT FK_FEE8BE67A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE land_borrow ADD CONSTRAINT FK_FEE8BE673147C936 FOREIGN KEY (people_id) REFERENCES user_people (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE land_borrow');
    }
}
