<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190625060933 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE transactions (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, budget_id INT DEFAULT NULL, category_id INT NOT NULL, note LONGTEXT DEFAULT NULL, created_at DATETIME NOT NULL, price BIGINT NOT NULL, INDEX IDX_EAA81A4CA76ED395 (user_id), INDEX IDX_EAA81A4C36ABA6B8 (budget_id), INDEX IDX_EAA81A4C12469DE2 (category_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE budgets (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, max_budget BIGINT NOT NULL, INDEX IDX_DCAA9548A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE incomes (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, note LONGTEXT NOT NULL, created_at DATETIME NOT NULL, price BIGINT NOT NULL, INDEX IDX_9DE2B5BDA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE land_borrow (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, people_id INT NOT NULL, note LONGTEXT NOT NULL, type VARCHAR(255) NOT NULL, is_pass TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, pass_at DATETIME DEFAULT NULL, price BIGINT NOT NULL, INDEX IDX_FEE8BE67A76ED395 (user_id), INDEX IDX_FEE8BE673147C936 (people_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transactions_categories (id INT AUTO_INCREMENT NOT NULL, created_by_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_F80A3544B03A8386 (created_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, username_canonical VARCHAR(180) NOT NULL, email VARCHAR(180) NOT NULL, email_canonical VARCHAR(180) NOT NULL, enabled TINYINT(1) NOT NULL, salt VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, last_login DATETIME DEFAULT NULL, confirmation_token VARCHAR(180) DEFAULT NULL, password_requested_at DATETIME DEFAULT NULL, roles LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', UNIQUE INDEX UNIQ_8D93D64992FC23A8 (username_canonical), UNIQUE INDEX UNIQ_8D93D649A0D96FBF (email_canonical), UNIQUE INDEX UNIQ_8D93D649C05FB297 (confirmation_token), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_people (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_4D44711BA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE transactions ADD CONSTRAINT FK_EAA81A4CA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE transactions ADD CONSTRAINT FK_EAA81A4C36ABA6B8 FOREIGN KEY (budget_id) REFERENCES budgets (id)');
        $this->addSql('ALTER TABLE transactions ADD CONSTRAINT FK_EAA81A4C12469DE2 FOREIGN KEY (category_id) REFERENCES transactions_categories (id)');
        $this->addSql('ALTER TABLE budgets ADD CONSTRAINT FK_DCAA9548A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE incomes ADD CONSTRAINT FK_9DE2B5BDA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE land_borrow ADD CONSTRAINT FK_FEE8BE67A76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE land_borrow ADD CONSTRAINT FK_FEE8BE673147C936 FOREIGN KEY (people_id) REFERENCES user_people (id)');
        $this->addSql('ALTER TABLE transactions_categories ADD CONSTRAINT FK_F80A3544B03A8386 FOREIGN KEY (created_by_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE user_people ADD CONSTRAINT FK_4D44711BA76ED395 FOREIGN KEY (user_id) REFERENCES `user` (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE transactions DROP FOREIGN KEY FK_EAA81A4C36ABA6B8');
        $this->addSql('ALTER TABLE transactions DROP FOREIGN KEY FK_EAA81A4C12469DE2');
        $this->addSql('ALTER TABLE transactions DROP FOREIGN KEY FK_EAA81A4CA76ED395');
        $this->addSql('ALTER TABLE budgets DROP FOREIGN KEY FK_DCAA9548A76ED395');
        $this->addSql('ALTER TABLE incomes DROP FOREIGN KEY FK_9DE2B5BDA76ED395');
        $this->addSql('ALTER TABLE land_borrow DROP FOREIGN KEY FK_FEE8BE67A76ED395');
        $this->addSql('ALTER TABLE transactions_categories DROP FOREIGN KEY FK_F80A3544B03A8386');
        $this->addSql('ALTER TABLE user_people DROP FOREIGN KEY FK_4D44711BA76ED395');
        $this->addSql('ALTER TABLE land_borrow DROP FOREIGN KEY FK_FEE8BE673147C936');
        $this->addSql('DROP TABLE transactions');
        $this->addSql('DROP TABLE budgets');
        $this->addSql('DROP TABLE incomes');
        $this->addSql('DROP TABLE land_borrow');
        $this->addSql('DROP TABLE transactions_categories');
        $this->addSql('DROP TABLE `user`');
        $this->addSql('DROP TABLE user_people');
    }
}
