<?php

namespace App\Form;

use App\Entity\LandBorrow;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LandBorrowType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('note')
            ->add('type' , ChoiceType::class , [
                'label' => 'طلبکاری یا بدهکاری',
                'choices' => [
                    'طلبکاری' => 'Lend',
                    'بدهکاری' => 'Borrow'
                ],
                'required' => true,
            ])
//            ->add('isPass')
//            ->add('createdAt')
//            ->add('passAt')
            ->add('price' , null , [
                'label' => 'مبلغ'
            ])
            ->add('user')
            ->add('people')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => LandBorrow::class,
        ]);
    }
}
