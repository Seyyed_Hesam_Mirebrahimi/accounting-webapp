<?php

namespace App\Form;

use App\Entity\Incomes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IncomesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('note' , null ,[
                'label' => 'یادداشت',
                'required' => true,
            ])
            ->add('price' , null ,[
                'label' => 'مبلغ',
                'required' => true,
            ])
//            ->add('createdAt')
//            ->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Incomes::class,
        ]);
    }
}
