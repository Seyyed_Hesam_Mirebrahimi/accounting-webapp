<?php

namespace App\Form;

use App\Entity\Budgets;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BudgetsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',null , ['label' => 'عنوان'])
            ->add('maxBudget',null , ['label' => 'بودجه مورد نظر به تومان'])
            ->add('user')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Budgets::class,
        ]);
    }
}
