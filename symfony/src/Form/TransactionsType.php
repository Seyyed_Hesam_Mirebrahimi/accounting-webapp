<?php

namespace App\Form;

use App\Entity\Transactions;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TransactionsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('note' , null , [
                'label' => 'یادداشت',
                'required' => true
            ])
            ->add('price' , null , [
                'label' => 'مبلغ',
                'required' => true
            ])
            ->add('user' , null , [
                'label' => 'کاربر'
            ])
            ->add('budget' , null , [
                'label' => 'انتخاب بودجه'
            ])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Transactions::class,
        ]);
    }
}
