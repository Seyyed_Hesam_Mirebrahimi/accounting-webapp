<?php
/**
 * @package iransans_mihanhosting
 * @version 1.0.0
 */
/*
Plugin Name: IranSans Font Package
Plugin URI: http://uniquewebco.ir
Description: IranSans FontPackage
Author: H Mirebrahimi
Version: 1.0.0
Author URI: http://uniquewebco.ir
*/
wp_enqueue_style('iransans_mihanhosting_font',plugins_url().'iransans/font.css');
